type Message is (
     ErrorMessage
   | AuthentificationMessage
   | ParameterStatusMessage
   | ReadyForQueryMessage
   | RowDescriptionMessage
   | DataRowMessage
   | CommandCompleteMessage
   | EmptyQueryResponseMessage
   | TerminateMessage
   | UnknownMessage
)

trait SerializableMessage
  fun dump(): ByteSeqIter
