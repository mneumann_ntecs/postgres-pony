primitive ErrorMessage is Stringable
  fun string() : String iso^ =>
    "ErrorMessage".string()
