primitive EmptyQueryResponseMessage is (Stringable & SerializableMessage)
  fun string() : String iso^ =>
    "EmptyQueryResponse".string()

  fun dump(): ByteSeqIter =>
    MessageEncoder('I').done()
