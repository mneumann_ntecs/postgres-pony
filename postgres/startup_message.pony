class val StartupMessage is SerializableMessage
  let proto_version: I32 = 3 << 16
  let params: Array[(String, String)] val

  new val create(params' : Array[(String, String)] val) =>
    params = params'

  fun dump(): ByteSeqIter =>
    MessageEncoder(None).>int32(proto_version).>key_value_pairs(params).done()
