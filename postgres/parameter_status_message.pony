primitive ParameterStatusMessage is Stringable
  fun string() : String iso^ =>
    "ParameterStatusMessage".string()
