use "buffered"
use "collections"

type Column is (Array[U8] val | None)

class val DataRowMessage is Stringable
  let columns : Array[Column] val

  new val create(columns': Array[Column] val) =>
    columns = columns'

  fun string() : String iso^ =>
    var s = "DataRowMessage("
    for column in columns.values() do
      match column
      | let c: Array[U8] val => s = s.add(String.from_array(c)).add(", ")
      end
    end
    s = s.add(")")
    s.string()

  fun dump(): ByteSeqIter =>
    MessageEncoder('D').>opt_byte_values(columns).done()

primitive DataRowMessageDecoder
  fun apply(body : Array[U8] iso) : DataRowMessage? =>
    let rd = Reader.>append(consume body)
    var n = rd.i16_be()?
    let columns = recover val
      let columns' = Array[Column].create()
      for i in Range[I16](0, n) do
        let sz = rd.i32_be()?
        if sz < 0 then
          columns'.push(None)
        else
          columns'.push(rd.block(sz.usize())?)
        end
      end
      columns'
    end
    DataRowMessage(columns)

