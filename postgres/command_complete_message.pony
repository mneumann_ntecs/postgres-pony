use "buffered"

class val CommandCompleteMessage is Stringable
  let cmd_tag : String val

  new val create(cmd_tag': String val) =>
    cmd_tag = cmd_tag'

  fun string() : String iso^ =>
    "CommandCompleteMessage(".add(cmd_tag).add(")").string()

  fun dump(): ByteSeqIter =>
    MessageEncoder('C').>string0(cmd_tag).done()

primitive CommandCompleteMessageDecoder
  fun apply(body : Array[U8] iso) : CommandCompleteMessage? =>
    let rd = Reader.>append(consume body)
    let cmd_tag = recover val
      String.from_array(rd.read_until(0)?)
    end
    CommandCompleteMessage(cmd_tag)
