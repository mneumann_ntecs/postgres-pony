primitive ReadyForQueryMessage is Stringable
  fun string() : String iso^ =>
    "ReadyForQueryMessage".string()
