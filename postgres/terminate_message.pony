primitive TerminateMessage is (Stringable & SerializableMessage)
  fun string() : String iso^ =>
    "TerminateMessage".string()

  fun dump(): ByteSeqIter =>
    MessageEncoder('X').done()
