use "buffered"

trait _MessageWriter
  fun ref writer() : Writer

  fun ref string0(str : String val) =>
    writer().>write(str).u8(0)

  fun ref key_value_pairs(pairs : Array[(String, String)] val) =>
    for (key, value) in pairs.values() do
      this.>string0(key).string0(value)
    end
    writer().u8(0)

  fun ref int16(n : I16) =>
    writer().i16_be(n)

  fun ref int32(n : I32) =>
    writer().i32_be(n)

  fun ref u8(n : U8) =>
    writer().u8(n)

  fun ref opt_byte_values(values : Array[(Array[U8] val | None)] val) =>
    this.>int16(values.size().i16())
    for value in values.values() do
      match value
      | None => this.int32(-1)
      | let v: Array[U8] val => this.sized_bytes(v)
      end
    end

  fun ref sized_bytes(bytes : Array[U8] val) =>
    writer().>i32_be(bytes.size().i32()).write(bytes)

class MessageEncoder is _MessageWriter
  var _writer : Writer
  let _msg_type : (U8 | None)

  fun ref writer() : Writer => _writer

  new create(msg_type : (U8 | None)) =>
    _msg_type = msg_type
    _writer = Writer

  fun ref done() : ByteSeqIter =>
    let body = _writer = Writer
    let wb = Writer
    match _msg_type
    | let msg_type : U8 => wb.u8(msg_type)
    end
    wb.i32_be(4 + body.size().i32())
    wb.writev(body.done())
    wb.done()

    /*
class StartupMessageEncoder is _MessageWriter
  var _writer : Writer

  fun ref writer() : Writer => _writer

  new create() =>
    _writer = Writer

  fun ref done() : ByteSeqIter =>
    let body = _writer = Writer
    let wb = Writer
    wb.i32_be(4 + body.size().i32())
    wb.writev(body.done())
    wb.done()
    */
