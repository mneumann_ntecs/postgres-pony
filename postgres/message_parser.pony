use "buffered"
use "debug"

primitive NeedsMoreData
primitive InvalidData

class MessageParser
  let _rd: Reader

  new create() =>
    _rd = Reader

  fun ref feed_data(data: Array[U8] iso) =>
    _rd.append(consume data)

  fun ref next_message(): (NeedsMoreData | InvalidData | Message) =>
    let msg_size = try _rd.peek_i32_be(1)? else return NeedsMoreData end

    if msg_size < 0 then
      Debug(["Negative message size"; msg_size])
      return InvalidData
    end

    if _rd.size() < (msg_size.usize() + 1) then
      return NeedsMoreData
    end

    try
      let ty = _rd.u8()?
      let sz = _rd.i32_be()?.usize()
      if sz < 4 then
        Debug(["Invalid message size"; sz])
        error
      end
      let msg_body = _rd.block(sz - 4)?

      match ty
      | 'E' => ErrorMessage
      | 'R' => AuthentificationMessage
      | 'S' => ParameterStatusMessage
      | 'Z' => ReadyForQueryMessage
      | 'I' => EmptyQueryResponseMessage
      | 'D' => DataRowMessageDecoder(consume msg_body)?
      | 'T' => RowDescriptionDecoder(consume msg_body)?
      | 'C' => CommandCompleteMessageDecoder(consume msg_body)?
      | 'X' => TerminateMessage
      else
        UnknownMessage(ty, consume msg_body)
      end
    else
      InvalidData
    end
