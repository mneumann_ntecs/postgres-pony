class val UnknownMessage is Stringable
  let _ty : U8
  let _body : Array[U8] val

  new val create(ty : U8, body : Array[U8] iso) =>
    _ty = ty
    _body = recover val consume body end

  fun string() : String iso^ =>
    "UnknownMessage(".add(_ty.string()).add(")").string()
