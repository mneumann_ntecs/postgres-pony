use "buffered"
use "collections"

class val RowDescriptionMessage is Stringable
  let fields : Array[FieldInfo] val

  new val create(fields': Array[FieldInfo] val) =>
    fields = fields'

  fun string() : String iso^ =>
    var s = "RowDescription("
    for field in fields.values() do
      s = s.add(field.string())
    end
    s = s.add(")")
    s.string()

class val FieldInfo is Stringable
  let name : String
  let oid : I32
  let attr_nr : I16
  let type_oid : I32
  let typlen : I16
  let atttypmod : I32
  let formatcode : I16

  new val create(name': String, oid': I32, attr_nr' : I16, type_oid': I32, typlen' : I16, atttypmod' : I32, formatcode' : I16) =>
    name = name'
    oid = oid'
    attr_nr = attr_nr'
    type_oid = type_oid'
    typlen = typlen'
    atttypmod = atttypmod'
    formatcode = formatcode'

  fun string() : String iso^ =>
    "{".add(name).add(", ").add(oid.string()).add(", ")
    .add(attr_nr.string()).add(", ")
    .add(type_oid.string())
    .add("}").string()

primitive RowDescriptionDecoder
  fun apply(body : Array[U8] iso) : RowDescriptionMessage? =>
    let fields = recover val
      let rd: Reader ref = Reader
      rd.append(consume body)
      var n = rd.i16_be()?.usize()
      let fields' = Array[FieldInfo].create(n)
      for i in Range(0, n) do
        fields'.push(FieldInfoDecoder(rd)?)
      end
      fields'
    end
    RowDescriptionMessage(fields)

primitive FieldInfoDecoder
  fun apply(rd : Reader ref) : FieldInfo val? =>
    let name = String.from_array(rd.read_until(0)?)
    let oid = rd.i32_be()?
    let attr_nr = rd.i16_be()?
    let type_oid = rd.i32_be()?
    let typlen = rd.i16_be()?
    let atttypmod = rd.i32_be()?
    let formatcode = rd.i16_be()?
    FieldInfo.create(name, oid, attr_nr, type_oid, typlen, atttypmod, formatcode)

