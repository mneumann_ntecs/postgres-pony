class val QueryMessage is SerializableMessage
  let query : String

  new val create(query': String) =>
    query = query'

  fun dump() : ByteSeqIter =>
    MessageEncoder('Q').>string0(query).done()
