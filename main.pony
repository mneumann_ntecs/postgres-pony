use "net"
use "buffered"
use "debug"
use "time"
use "postgres"

trait DatabaseHandler
  be connected()
  be message(msg : Message val)

actor DummyConnection is DatabaseHandler
  new create() =>
    None

  be connected() =>
    None

  be message(msg : Message val) =>
    Debug(msg)
    None

trait ResponseChannel
  fun val send_message(msg : SerializableMessage val)

class val DatabaseResponseChannel is ResponseChannel
  let _conn: TCPConnection tag

  new val create(conn: TCPConnection tag) =>
    _conn = conn

  fun val send_message(msg : SerializableMessage val) =>
    _conn.writev(msg.dump())

actor DatabaseConnection is DatabaseHandler
  let _response_channel: ResponseChannel val
  let _startup_message: StartupMessage val
  var _cnt : USize = 0

  new create(response_channel: ResponseChannel val, startup_message : StartupMessage val) =>
    _response_channel = response_channel
    _startup_message = startup_message

  fun _send_message(msg : SerializableMessage val) =>
    _response_channel.send_message(msg)

  be connected() =>
    Debug("Connected")
    _send_message(_startup_message)

  be message(msg : Message val) =>
    Debug(_cnt = _cnt + 1)
    Debug(msg)
    match msg
    | TerminateMessage => Debug("Terminate")
    | ReadyForQueryMessage =>
      if _cnt < 10000 then
        _send_message(QueryMessage("select * from accounts"))
      else
        Debug("Sending terminate message")
        _send_message(TerminateMessage)
      end
    else
      None
    end


class PostgresConnectionNotify is TCPConnectionNotify
  let _message_parser: MessageParser
  var _actor : DatabaseHandler tag
  let _startup_message : StartupMessage val

  new create(startup_message : StartupMessage val) =>
    _message_parser = MessageParser
    _actor = DummyConnection
    _startup_message = startup_message

  fun ref connected(conn: TCPConnection ref) =>
    _actor = DatabaseConnection(DatabaseResponseChannel(conn), _startup_message)
    _actor.connected()

  fun ref received(
    conn: TCPConnection ref,
    data: Array[U8] iso,
    times: USize)
    : Bool
  =>
    _message_parser.feed_data(consume data)
    while true do
      match _message_parser.next_message()
      | NeedsMoreData => break
      | InvalidData =>
          Debug("Message contains invalid data")
          conn.close()
      | let msg: TerminateMessage =>
          Debug("Received TerminateMessage. Closing connection")
          _actor.message(msg)
          conn.close()
      | let msg: Message => _actor.message(msg)
      end
    end
    false

  fun ref closed(
    conn: TCPConnection ref)
    : None val =>
      Debug("Connection closed")
      None

  fun ref connect_failed(conn: TCPConnection ref) =>
    Debug("connect failed")
    None

actor Main
  new create(env: Env) =>
    try
      let user = env.args(1)?
      let database = env.args(2)?
      TCPConnection(env.root as AmbientAuth,
          recover PostgresConnectionNotify(StartupMessage([ ("user", user); ("database", database) ])) end,
          "localhost",
          "5432")
    end
